package br.com.zup.ListaTelefonica.Servico;

import br.com.zup.Contato.Contato;

public class Sistema {

    //
    public static void cadastrarContato() throws Exception{

        IO.mostrarTexto("Digite o seu nome: ");
        String nome = IO.criaScanner().nextLine();

        IO.mostrarTexto("Digite o seu telefone: ");
        String telefone = IO.criaScanner().nextLine();

        IO.mostrarTexto("Digite o seu email: ");
        String email = IO.criaScanner().nextLine();


        Contato usuarioDigita = new Contato(nome, telefone, email);
        ListaDeContato.adicionarUmContato(usuarioDigita);


    }

    public static void excluirContatoPorEmail() throws Exception{
        System.out.println("Digite o email que deseja excluir");
        String email = IO.criaScanner().nextLine();
        ListaDeContato.excluirContato(email);

    }


    public static void menu() {

        System.out.println("-------------");
        System.out.println("Digite 1 para cadastrar um contato");
        System.out.println("Digite 2 para exibir os contatos cadastrados");
        System.out.println("Digite 3 para excluir um contato da lista");
        System.out.println("Digite 4 para sair do programa");
        System.out.println("-------------");

    }


    public static void executarSistema() throws Exception {
        boolean executar = true;

        while (executar) {
            menu();
            String opcaoEscolhida = IO.criaScanner().next();

            switch (opcaoEscolhida) {

                case "1":
                    cadastrarContato();
                    break;

                case "2":
                    ListaDeContato.mostrarListaDeContatos();
                    break;

                case "3":
                    excluirContatoPorEmail();
                    break;

                case "4":
                    executar = false;
                    break;
            }

        }
    }
}

//    public static void executarSistema() throws Exception{
//        boolean executar = true;
//
//        while (executar){
//            menu();
//            String opcaoEscolhida = LeituraDeDados.criaScanner().next();
//
//            switch (opcaoEscolhida){
//                case "1":
//                    cadastrarContato();
//                    break;
//
//                case "2":
//                    ListaDeContato.mostrarListaDeContatos();
//                    break;
//
//                case "3":
//                    excluirContatoPorEmail();
//                    break;
//
//                case "4":
//                    executar = false;
//                    break;
//            }
//
//        }



