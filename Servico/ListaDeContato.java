package br.com.zup.ListaTelefonica.Servico;

import br.com.zup.Contato.Contato;

import java.util.ArrayList;
import java.util.List;

public class ListaDeContato {

    //Atributo contato dos usuarios, onde serão guardados os emails.
    private static List<Contato> contatosDosUsuarios = new ArrayList<>();


    //Metódo para cadastrar um novo contato.
    public static void adicionarUmContato(Contato criarContato) throws Exception{

        validarContatoPorEmail(criarContato.getEmail()); //Validando meu e-mail pegando o meu parametro e passando meu e-mail.
        contatosDosUsuarios.add(criarContato); // Adicionei um novo contato a minha lista contato.

    }

    //Metódo para mostrar a lista de contatos...
    public static void mostrarListaDeContatos(){

        System.out.println("Lista de Contatos: ");
        //Um for para pecorrer a listaDeContatos onde mostra os mesmos...
        for (Contato pecorrendoListaDeContatos: contatosDosUsuarios) {
            System.out.println(pecorrendoListaDeContatos);
        }

    }

    //Metódo para excluir o contato...
    public static void excluirContato(String email) throws Exception{

        Contato emails = null;

        for (Contato excluirContatos: contatosDosUsuarios) {
            if (excluirContatos.getEmail().equals(email)) {
               emails = excluirContatos;
                System.out.println("E-mail excluido com sucesso! ");
            }
        }

        contatosDosUsuarios.remove(emails);
        throw new Exception("Email não cadastrado impossivel excluir");
    }

    //Metódo para validar email
    public static void validarContatoPorEmail(String email) throws Exception{

        for (Contato validarEmail: contatosDosUsuarios) {
            if (validarEmail.getEmail().equals(email)) {
//                System.out.println("Email já existe! ");
                throw new Exception("Email já existe!");
            }
        }

    }



}
