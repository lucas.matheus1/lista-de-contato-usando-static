package br.com.zup.ListaTelefonica.Dominio;

public class Contato {

    //Atributos
    private String nome, telefone, email;

    //Metódo Construtor...
    public Contato(){}

    public Contato(String nome, String telefone, String email) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
    }

    //Metódo seletores e modificadores...
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("O nome do contato é "+ this.nome);
        string.append("\nO telefone do contato é "+ this.telefone);
        string.append("\nO email do contato é "+ this.email);
        return string.toString();
    }

}
